package personal.bankingapp.accountstatement;

import lombok.Value;
import org.springframework.hateoas.Resource;
import personal.bankingapp.transaction.Transaction;

import java.util.List;

@Value
class AccountStatementResource {
    private Integer balance;
    private List<Resource<Transaction>> transactions;
}

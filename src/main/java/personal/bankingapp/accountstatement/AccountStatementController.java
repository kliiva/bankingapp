package personal.bankingapp.accountstatement;

import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountStatementController {

    private final AccountStatementService accountStatementService;
    private final AccountStatementResourceAssembler assembler;

    public AccountStatementController(AccountStatementService accountStatementService, AccountStatementResourceAssembler assembler) {
        this.accountStatementService = accountStatementService;
        this.assembler = assembler;
    }

    @GetMapping("/accounts/{id}/statement")
    public Resource<AccountStatementResource> one(@PathVariable Long id) {
        AccountStatement accountStatement = accountStatementService.statementByAccountId(id);
        return assembler.toResource(accountStatement);
    }
}

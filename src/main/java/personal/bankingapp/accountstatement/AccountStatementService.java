package personal.bankingapp.accountstatement;

import org.springframework.stereotype.Service;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountService;
import personal.bankingapp.transaction.Transaction;
import personal.bankingapp.transaction.TransactionService;

import java.util.List;

@Service
public class AccountStatementService {

    private final AccountService accountService;
    private final TransactionService transactionService;

    public AccountStatementService(AccountService accountService, TransactionService transactionService) {
        this.accountService = accountService;
        this.transactionService = transactionService;
    }

    public AccountStatement statementByAccountId(Long id) {
        Account account = accountService.findById(id);
        List<Transaction> transactions = transactionService.getAccountTransactions(account);
        return new AccountStatement(account, transactions);
    }

}

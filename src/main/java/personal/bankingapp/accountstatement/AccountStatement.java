package personal.bankingapp.accountstatement;

import lombok.AllArgsConstructor;
import lombok.Data;
import personal.bankingapp.account.Account;
import personal.bankingapp.transaction.Transaction;

import java.util.List;

@Data
@AllArgsConstructor
class AccountStatement {
    private Account account;
    private List<Transaction> transactions;
}

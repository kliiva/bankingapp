package personal.bankingapp.accountstatement;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;
import personal.bankingapp.transaction.Transaction;
import personal.bankingapp.transaction.TransactionResourceAssembler;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class AccountStatementResourceAssembler  implements ResourceAssembler<AccountStatement, Resource<AccountStatementResource>> {

    private final TransactionResourceAssembler transactionResourceAssembler;

    public AccountStatementResourceAssembler(TransactionResourceAssembler transactionResourceAssembler) {
        this.transactionResourceAssembler = transactionResourceAssembler;
    }

    @Override
    public Resource<AccountStatementResource> toResource(AccountStatement accountStatement) {
        List<Resource<Transaction>> transactions = accountStatement.getTransactions().stream()
                .map(transactionResourceAssembler::toResource)
                .collect(Collectors.toList());
        return new Resource<>(new AccountStatementResource(accountStatement.getAccount().getBalance(), transactions),
                linkTo(methodOn(AccountStatementController.class).one(accountStatement.getAccount().getId())).withSelfRel());
    }
}

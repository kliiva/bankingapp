package personal.bankingapp.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountRepository;
import personal.bankingapp.transaction.Transaction;
import personal.bankingapp.transaction.TransactionRepository;

@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(AccountRepository accountRepository, TransactionRepository transactionRepository) {
        return args -> {
            Account account1 = new Account(100);
            Account account2 = new Account(200);
            Account account3 = new Account(200);
            accountRepository.save(account1);
            accountRepository.save(account2);
            accountRepository.save(account3);
            Transaction transaction1 = new Transaction(account1, account2, 10);
            Transaction transaction2 = new Transaction(account1, account2, 20);
            transactionRepository.save(transaction1);
            transactionRepository.save(transaction2);
            log.info("Preloading: " + account1);
            log.info("Preloading: " + account2);
            log.info("Preloading: " + transaction1);
            log.info("Preloading: " + transaction2);
        };
    }
}

package personal.bankingapp.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    // needs to be query to ensure atomicity of read-write (Spring's Transactional on methods only applies to writes
    // drops all non-flushed changes in the entity manager (not relevant to this app though, but
    // see https://stackoverflow.com/questions/32258857/spring-boot-data-jpa-modifying-update-query-refresh-persistence-context)
    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update Account a set a.balance=a.balance+:amountDelta where a = :account")
    void addDeltaToAccountAmount(Account account, int amountDelta);
}

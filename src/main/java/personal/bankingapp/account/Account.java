package personal.bankingapp.account;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @NotNull
    @Min(0)
    private Integer balance;

    public Account(Integer balance) {
        this.balance = balance;
    }

    public boolean hasSameId(Account other) {
        return id.longValue() == other.id.longValue();
    }
}

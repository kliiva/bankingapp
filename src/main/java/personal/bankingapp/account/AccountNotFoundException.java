package personal.bankingapp.account;

class AccountNotFoundException extends RuntimeException{

    AccountNotFoundException(Long id) {
        super(String.format("Could not find account '%s'", id));
    }
}

package personal.bankingapp.account;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import personal.bankingapp.transaction.TransactionException;

import java.util.List;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Account findById(Long id) {
        return accountRepository.findById(id).orElseThrow(() -> new AccountNotFoundException(id));
    }

    public void updateAccountBalance(Account account, int amountDelta) {
        try {
            accountRepository.addDeltaToAccountAmount(account, amountDelta);
        } catch (DataIntegrityViolationException e) {
            throw new TransactionException("Not enough founds on the account");
        }
    }
}

package personal.bankingapp.transaction;

import org.springframework.stereotype.Service;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TransactionService {

    private final TransactionRepository repository;

    private final AccountService accountService;

    public TransactionService(TransactionRepository repository, AccountService accountService) {
        this.repository = repository;
        this.accountService = accountService;
    }

    public List<Transaction> findAll() {
        return repository.findAll();
    }

    public Transaction findById(Long id) {
        return repository.findById(id).orElseThrow(() -> new TransactionNotFoundException(id));
    }

    public List<Transaction> getAccountTransactions(Account account) {
        return repository.findAccountTransactions(account);
    }

    //transactional do ensure that the sum of the money in the system is the same at any given time
    @Transactional
    public Transaction newTransaction(Transaction newTransaction) {
        validateNewTransaction(newTransaction);

        Account fromAccount = accountService.findById(newTransaction.getFromAccount().getId());
        Account toAccount = accountService.findById(newTransaction.getToAccount().getId());

        accountService.updateAccountBalance(fromAccount, -newTransaction.getAmount());
        accountService.updateAccountBalance(toAccount, newTransaction.getAmount());

        return repository.save(newTransaction);
    }

    private void validateNewTransaction(Transaction newTransaction) {
        if (newTransaction.getAmount() <= 0) {
            throw new TransactionException("Transaction amount must be positive");
        }
        if (newTransaction.getFromAccount() == null) {
            throw new TransactionException("From-account missing in transaction");
        }
        if (newTransaction.getToAccount() == null) {
            throw new TransactionException("To-account missing in transaction");
        }
        if (newTransaction.getFromAccount().hasSameId(newTransaction.getToAccount())) {
            throw new TransactionException("Can't make a transaction from the account to itself");
        }
    }
}

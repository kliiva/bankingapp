package personal.bankingapp.transaction;

public class TransactionException extends RuntimeException {
    public TransactionException(String msg) {
        super(msg);
    }
}

package personal.bankingapp.transaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import personal.bankingapp.account.Account;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query("SELECT t FROM Transaction t WHERE  t.toAccount = :account OR t.fromAccount = :account")
    List<Transaction> findAccountTransactions(Account account);
}

package personal.bankingapp.transaction;

import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class TransactionController {

    private final TransactionService service;
    private final TransactionResourceAssembler assembler;

    public TransactionController(TransactionService service, TransactionResourceAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    @GetMapping("/transactions/{id}")
    Resource<Transaction> one(@PathVariable Long id) {
        return assembler.toResource(service.findById(id));
    }

    @PostMapping("/transactions")
    ResponseEntity<Resource<Transaction>> newTransaction(@RequestBody Transaction newTransaction) throws URISyntaxException {
        Transaction transaction = service.newTransaction(newTransaction);
        Resource<Transaction> transactionResource = assembler.toResource(transaction);
        return ResponseEntity
                .created(new URI(transactionResource.getId().expand().getHref()))
                .body(transactionResource);
    }
}

package personal.bankingapp.transaction;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class TransactionResourceAssembler implements ResourceAssembler<Transaction, Resource<Transaction>> {
    @Override
    public Resource<Transaction> toResource(Transaction transaction) {
        return new Resource<>(transaction,
                linkTo(methodOn(TransactionController.class).one(transaction.getId())).withSelfRel()
        );
    }
}

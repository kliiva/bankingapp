package personal.bankingapp.transaction;

class TransactionNotFoundException extends TransactionException{
    TransactionNotFoundException(Long id) {
        super(String.format("Could not find transaction '%s'", id));
    }
}

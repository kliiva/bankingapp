package personal.bankingapp.accountstatement;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountService;
import personal.bankingapp.transaction.Transaction;
import personal.bankingapp.transaction.TransactionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountStatementServiceTest {

    @Mock
    AccountService accountService;

    @Mock
    TransactionService transactionService;

    @InjectMocks
    AccountStatementService accountStatementService;


    @Test
    public void testStatementByAccountId() {
        Account account1 = new Account(1L, 1000);
        Account account2 = new Account(2L, 20);
        when(accountService.findById(1L)).thenReturn(account1);
        List<Transaction> transactions = Arrays.asList(
                new Transaction(3L, account1, account2, 10),
                new Transaction(4L, account2, account1, 20));
        when(transactionService.getAccountTransactions(account1)).thenReturn(transactions);
        AccountStatement expected = new AccountStatement(account1, new ArrayList<>(transactions));

        AccountStatement actual = accountStatementService.statementByAccountId(1L);

        assertEquals(expected, actual);
    }

    @Test
    public void testStatementByAccountIdNoTransactions() {
        Account account1 = new Account(1L, 1000);
        when(accountService.findById(1L)).thenReturn(account1);
        List<Transaction> transactions = new ArrayList<>();
        when(transactionService.getAccountTransactions(account1)).thenReturn(transactions);
        AccountStatement expected = new AccountStatement(account1, new ArrayList<>(transactions));

        AccountStatement actual = accountStatementService.statementByAccountId(1L);

        assertEquals(expected, actual);
    }
}
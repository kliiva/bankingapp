package personal.bankingapp.accountstatement;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountRepository;
import personal.bankingapp.transaction.Transaction;
import personal.bankingapp.transaction.TransactionRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AccountStatementControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    AccountRepository accountRepository;

    @MockBean
    TransactionRepository transactionRepository;

    @MockBean
    AccountStatementService accountStatementService;

    @Test
    public void testOneOk() throws Exception {
        Account account1 = new Account(1L, 100);
        Account account2 = new Account(2L, 100);
        List<Transaction> transactions = Arrays.asList(
                new Transaction(3L, account1, account2, 10),
                new Transaction(4L, account2, account1, 20));

        when(accountStatementService.statementByAccountId(1L)).thenReturn(new AccountStatement(account1, transactions));

        mockMvc.perform(get("/accounts/1/statement")
                .header("Accept", MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance", is(100)))
                .andExpect(jsonPath("$.transactions", hasSize(2)))
                .andExpect(jsonPath("$.transactions[0].id", is(3)))
                .andExpect(jsonPath("$.transactions[0].fromAccount.id", is(1)))
                .andExpect(jsonPath("$.transactions[0].toAccount.id", is(2)))
                .andExpect(jsonPath("$.transactions[0].amount", is(10)))
                .andExpect(jsonPath("$.transactions[0]._links.self.href", is("http://localhost/transactions/3")))
                .andExpect(jsonPath("$.transactions[1].id", is(4)))
                .andExpect(jsonPath("$.transactions[1].fromAccount.id", is(2)))
                .andExpect(jsonPath("$.transactions[1].toAccount.id", is(1)))
                .andExpect(jsonPath("$.transactions[1].amount", is(20)))
                .andExpect(jsonPath("$.transactions[1]._links.self.href", is("http://localhost/transactions/4")))
                .andExpect(jsonPath("$._links.self.href", is("http://localhost/accounts/1/statement")));

        verify(accountStatementService, times(1)).statementByAccountId(1L);

    }

    @Test
    public void testOneNoTransactions() throws Exception {
        List<Transaction> transactions = new ArrayList<>();

        when(accountStatementService.statementByAccountId(1L)).thenReturn(new AccountStatement(new Account(1L, 100), transactions));

        mockMvc.perform(get("/accounts/1/statement")
                .header("Accept", MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance", is(100)))
                .andExpect(jsonPath("$.transactions", hasSize(0)))
                .andExpect(jsonPath("$._links.self.href", is("http://localhost/accounts/1/statement")));

        verify(accountStatementService, times(1)).statementByAccountId(1L);
    }
}
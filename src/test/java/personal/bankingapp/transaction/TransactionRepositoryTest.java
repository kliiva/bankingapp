package personal.bankingapp.transaction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountRepository;

import javax.validation.ConstraintViolationException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TransactionRepositoryTest {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    AccountRepository accountRepository;

    @Test
    public void testFindAccountTransactions() {
        Account account1 = accountRepository.save(new Account(10));
        Account account2 = accountRepository.save(new Account(20));
        Account account3 = accountRepository.save(new Account(30));

        Transaction transaction1 = transactionRepository.save(new Transaction(account1, account2, 40));
        Transaction transaction2 = transactionRepository.save(new Transaction(account1, account3, 50));
        Transaction transaction3 = transactionRepository.save(new Transaction(account2, account1, 60));
        Transaction transaction4 = transactionRepository.save(new Transaction(account2, account3, 70));
        Transaction transaction5 = transactionRepository.save(new Transaction(account3, account1, 80));
        Transaction transaction6 = transactionRepository.save(new Transaction(account3, account2, 90));

        List<Transaction> accountTransactions = transactionRepository.findAccountTransactions(account1);

        assertThat(accountTransactions, hasSize(4));
        assertThat(accountTransactions, contains(transaction1, transaction2, transaction3, transaction5));
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSave() {
        Account account1 = accountRepository.save(new Account(10));
        Account account2 = accountRepository.save(new Account(20));
        transactionRepository.save(new Transaction(account1, account2, -10));
        System.out.println(transactionRepository.findAll());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSaveFromAccountNull() {
        Account account2 = accountRepository.save(new Account(20));
        transactionRepository.save(new Transaction(null, account2, null));
        System.out.println(transactionRepository.findAll());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSaveToAccountNull() {
        Account account1 = accountRepository.save(new Account(10));
        transactionRepository.save(new Transaction(account1, null, 5));
        System.out.println(transactionRepository.findAll());
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSaveAmountNull() {
        Account account1 = accountRepository.save(new Account(10));
        Account account2 = accountRepository.save(new Account(20));
        transactionRepository.save(new Transaction(account1, account2, null));
        System.out.println(transactionRepository.findAll());
    }
}
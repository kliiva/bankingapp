package personal.bankingapp.transaction;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import personal.bankingapp.account.Account;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TransactionService service;

    @Autowired
    TransactionResourceAssembler assembler;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testOK() throws Exception {
        Transaction transaction = new Transaction(3L, new Account(1L, 100), new Account(2L, 100), 10);

        when(service.findById(1L)).thenReturn(transaction);

        mockMvc.perform(get("/transactions/1")
                .header("Accept", MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.fromAccount.id", is(1)))
                .andExpect(jsonPath("$.toAccount.id", is(2)))
                .andExpect(jsonPath("$.amount", is(10)))
                .andExpect(jsonPath("$._links.self.href", is("http://localhost/transactions/3")))
        ;

        verify(service, times(1)).findById(1L);
    }

    @Test
    public void testNewTransactionOk() throws Exception {
        Transaction transaction1 = new Transaction(3L, new Account(1L, 100), new Account(2L, 100), 10);

        when(service.newTransaction(any(Transaction.class))).thenReturn(transaction1);

        mockMvc.perform(post("/transactions")
                .header("Accept", MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(transaction1)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.fromAccount.id", is(1)))
                .andExpect(jsonPath("$.toAccount.id", is(2)))
                .andExpect(jsonPath("$.amount", is(10)))
                .andExpect(jsonPath("$._links.self.href", is("http://localhost/transactions/3")))
        ;

        verify(service, times(1)).newTransaction(any(Transaction.class));
    }
}
package personal.bankingapp.transaction;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import personal.bankingapp.account.Account;
import personal.bankingapp.account.AccountService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountService accountService;

    @InjectMocks
    private TransactionService transactionService;

    @Test
    public void testGetAccountTransactions() {
        Account account = new Account();
        List<Transaction> transactions = new ArrayList<>();

        when(transactionRepository.findAccountTransactions(account)).thenReturn(transactions);

        assertSame(transactions, transactionService.getAccountTransactions(account));
    }

    @Test
    public void testNewTransaction() {
        Account account1 = new Account(1L, 10);
        Account account2 = new Account(2L, 20);
        Transaction newTransaction = new Transaction(account1, account2, 5);

        when(accountService.findById(1L)).thenReturn(account1);
        when(accountService.findById(2L)).thenReturn(account2);

        transactionService.newTransaction(newTransaction);

        verify(accountService, times(1)).updateAccountBalance(account1, -5);
        verify(accountService, times(1)).updateAccountBalance(account2, 5);
        verify(transactionRepository, times(1)).save(newTransaction);
    }

    @Test(expected = TransactionException.class)
    public void testNewTransactionNegativeAmount() {
        transactionService.newTransaction(new Transaction(null, null, -1));
    }

    @Test(expected = TransactionException.class)
    public void testNewTransactionSameIds() {
        transactionService.newTransaction(new Transaction(new Account(1L, 10), new Account(1L, 20), 1));
    }

    @Test(expected = TransactionException.class)
    public void testNewTransactionNoFromAccount() {
        transactionService.newTransaction(new Transaction(null, null, 10));
    }

    @Test(expected = TransactionException.class)
    public void testNewTransactionNoToAccount() {
        transactionService.newTransaction(new Transaction(new Account(), null, 10));
    }
}
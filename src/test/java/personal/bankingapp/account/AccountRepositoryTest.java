package personal.bankingapp.account;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AccountRepositoryTest {

    @Autowired
    AccountRepository accountRepository;

    @Test
    public void testAddDeltaToAccountAmountPositiveAmount() {
        Account account = accountRepository.save(new Account(100));
        accountRepository.addDeltaToAccountAmount(account, 50);

        assertEquals(150L, accountRepository.findById(account.getId()).get().getBalance().longValue());
    }

    @Test
    public void testAddDeltaToAccountAmountNegativeAmount() {
        Account account = accountRepository.save(new Account(100));
        accountRepository.addDeltaToAccountAmount(account, -50);

        assertEquals(50L, accountRepository.findById(account.getId()).get().getBalance().longValue());
    }

    @Test
    public void testAddDeltaToAccountAmountNegativeFullAmount() {
        Account account = accountRepository.save(new Account(100));
        accountRepository.addDeltaToAccountAmount(account, -100);

        assertEquals(0L, accountRepository.findById(account.getId()).get().getBalance().longValue());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void testAddDeltaToAccountAmountNotEnoughFounds() {
        Account account = accountRepository.save(new Account(100));
        accountRepository.addDeltaToAccountAmount(account, -150);
    }

}
package personal.bankingapp.account;

import org.junit.Test;

import static org.junit.Assert.*;

public class AccountTest {

    @Test
    public void testHasDifferentId() {
        Account account1 = new Account(new Long(1L), 0);
        Account account2 = new Account(new Long(1L), 0);

        assert account1.getId() != account2.getId();

        assertTrue(account1.hasSameId(account2));
    }
}
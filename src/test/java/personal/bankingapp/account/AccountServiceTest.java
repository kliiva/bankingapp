package personal.bankingapp.account;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import personal.bankingapp.transaction.TransactionException;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    AccountRepository accountRepository;

    @InjectMocks
    AccountService accountService;

    @Test
    public void testById() {
        Account account = new Account(1L,1000);
        when(accountRepository.findById(1L)).thenReturn(Optional.of(account));

        Account actual = accountService.findById(1L);

        assertEquals(account, actual);
        verify(accountRepository, times(1)).findById(1L);
    }

    @Test(expected = AccountNotFoundException.class)
    public void testByIdNotFound() {
        when(accountRepository.findById(1L)).thenReturn(Optional.empty());
        accountService.findById(1L);
    }

    @Test
    public void testUpdateAccountBalance() {
        Account account = new Account(1L, 10);
        accountService.updateAccountBalance(account, 5);
        verify(accountRepository, times(1)).addDeltaToAccountAmount(account, 5);
    }

    @Test(expected = TransactionException.class)
    public void testUpdateAccountBalanceNotEnoughFounds() {
        Account account = new Account(1L, 1);

        doThrow(new DataIntegrityViolationException("msg")).when(accountRepository).addDeltaToAccountAmount(account, 2);

        accountService.updateAccountBalance(account, 2);
    }
}
# REST Banking API Demo

A simple REST API for a small banking app. Provides Both entities have a property called `name` and are identified by a property called `id`.

## API structure

API uses the url context `/api/v1` and supports the following urls and methods

1. `/api/v1/account/{id}/statement` (`GET`)   
2. `/api/v1/transactions` (`POST`)   
3. `/api/v1/transactions/{id}` (`GET`)

`/api/v1/account/{id}/statement` returns the account statement (identified by `id` parameter) which consists of the balance of the account and transactions involving it.
 
`/api/v1/transactions` is used to make a transfer between two accounts.

`/api/v1/transactions/{id}` returns the statement (identified by `id` parameter) which includes the id of the account 
from which the transaction was made, the id of the account to which it was made and the amount.

Neither requests require authentication.

## Prerequisites

The project assumes Java (>=1.8) (and optionally Maven).

## Demo application

The system can be started with commands

```
./mvnw clean spring-boot:run
```

or (if Maven installed) with the command 
```
mvn clean spring-boot:run
```
or from inside IDE by running the class `RestDemoApplication`.

The demo comes with preloaded:

* accounts (with ids 1, 2, 3)
* transactions (from account 1 to account 2 with the amount of 10 and from account 2 to account 1 with the amount of 20)

The demo application uses the port 8080.

An example of a full url is

```
http://localhost:8080/api/v1/accounts/1/statement
```

An example of JSON used to make a new transaction with `/api/v1/transactions` is

```
{"toAccount": {"id": 2},"fromAccount": {"id": 1},"amount": 1}
```

The demo application uses H2 database so it doesn't persist entities between different execution of it.

## Running the tests

Tests can be executed with the command 
```
./mvnw test
```

or (if Maven installed) with the command 
```
mvn test
```

### Purpose of the tests

Tests cover all the machinery associated with the aforementioned requests, both valid and invalid use of the API. 

## Built With

* [Spring Boot](https://spring.io/projects/spring-boot) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [H2 Database Engine](https://www.h2database.com/html/main.html) (created entities and associations are lost after stopping the demo)
